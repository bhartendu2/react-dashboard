
import React, { useState} from 'react';
import { useNavigate } from "react-router-dom";
import "./Login.css";


function Login() {
  const navigate = useNavigate()

  const initialValues = { email: "", password: ""};
  const [users, setUsers] = useState(initialValues);

  const handleChange = (e) => {
    setUsers({ ...users, [e.target.name] : e.target.value})
  };

 

  return (
    <div className='mainContent' >
    <br></br><br></br><br></br><br></br>
      <form className='form-login'> 
        <div className='form-inner'>
          <h2>Login</h2>
          <br/>
    
          <div className='form-group'>
            <label htmlFor='email'>Email:</label>
            <input type="email" name="email" id="email" value={users.email} onChange={handleChange }/>
           
          </div>
          <div className='form-group'>
            <label htmlFor='password'>Password:</label>
            <input type="password" name="password" id="password" value={users.password} onChange={handleChange}/>
           
          </div> 
            <button className="btn" type="submit" onClick={()=> navigate("home")} >Login

            </button>
            
        </div>
      </form>
    </div>
  )
}

export default Login;
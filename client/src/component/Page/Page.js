
import "./Page.css"
import SalesLogo from "../../Icons/totalsales.png";
import CustomerLogo from "../../Icons/icons8-add-user-group-man-man-48.png";
import ProductsIcon from "../../Icons/icons8-grocery-shelf-30.png";
import RevenueLogo from "../../Icons/transaction.png"


export default function Page() {

          return (
                    <div>
                              <div style={{ display: 'flex', marginLeft: "50px", marginTop: 13 }}>
                                        <div className='introContainer'>
                                                  <div style={{ width: '30%' }}>
                                                            <p style={{ textAlign: 'start', color: "white", marginLeft: 15 }}>Hy! bhartendu</p>
                                                            <p style={{ textAlign: 'start', color: "#1BDCA2", marginLeft: 15, marginTop: -12, fontSize: 12 }}>You get a crazy growth </p>
                                                            <p style={{ textAlign: 'start', color: "white", marginLeft: 15, fontSize: "13px", marginTop: 40 }}><span>$302k</span> Dollar</p>
                                                            <button style={{ paddingLeft: "25px", paddingRight: '25px', borderRadius: "10px", backgroundColor: "green", color: "white", marginRight: '29%', paddingTop: 4, paddingBottom: 4 }}>View</button>
                                                  </div>
                                                  <div style={{ width: "60%", marginTop: "-23%" }}>
                                                           
                                                  </div>

                                        </div>
                                        <div className='itemsContainer'>
                                                  <p style={{ textAlign: "start", marginLeft: "40px", color: "white" }}>Statistics</p>
                                                  <div style={{ display: "flex", marginLeft: "40px" }}>

                                                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                                                      <div style={{ width: "40px", height: '40px', backgroundColor: "black", borderRadius: "50%" }}>
                                                                                <img src={`${SalesLogo}`} style={{ width: "25px", height: "25px", marginTop: 5 }} alt="" />
                                                                      </div>
                                                                      <div>
                                                                                <p style={{ color: "#1BDCA2", marginLeft: "10px", fontSize: 13, marginTop: 20 }}>$400k</p>
                                                                                <p style={{ color: "#A5A1A1", marginLeft: "10px", marginTop: "-14px" }}>Sales</p>
                                                                      </div>
                                                            </div>
                                                            <div style={{ display: 'flex', alignItems: 'center', marginLeft: "50px" }}>
                                                                      <div style={{ width: "40px", height: '40px', backgroundColor: "black", borderRadius: "50%" }}>
                                                                                <img src={`${CustomerLogo}`} style={{ width: "25px", height: "25px", marginTop: 5 }} alt="" />
                                                                      </div>
                                                                      <div>
                                                                                <p style={{ color: "#1BDCA2", marginLeft: "10px", fontSize: 13, marginTop: 20, textAlign: 'start' }}>493K</p>
                                                                                <p style={{ color: "#A5A1A1", marginLeft: "10px", marginTop: "-14px" }}>Customer</p>
                                                                      </div>
                                                            </div>
                                                            <div style={{ display: "flex", alignItems: 'center', marginLeft: "50px" }}>
                                                                      <div style={{ width: "40px", height: '40px', backgroundColor: "black", borderRadius: "50%" }}>
                                                                                <img src={`${ProductsIcon}`} style={{ width: "25px", height: "25px", marginTop: 5 }} alt="" />
                                                                      </div>
                                                                      <div>
                                                                                <p style={{ color: "#1BDCA2", marginLeft: "10px", fontSize: 13, marginTop: 20, textAlign: 'start' }}>40K</p>
                                                                                <p style={{ color: "#A5A1A1", marginLeft: "10px", marginTop: "-14px" }}>Products</p>
                                                                      </div>
                                                            </div>
                                                            <div style={{ display: "flex", alignItems: 'center', marginLeft: "50px" }}>
                                                                      <div style={{ width: "40px", height: '40px', backgroundColor: "black", borderRadius: "50%" }}>
                                                                                <img src={`${RevenueLogo}`} style={{ width: "25px", height: "25px", marginTop: 7 }} alt="" />
                                                                      </div>
                                                                      <div>
                                                                                <p style={{ color: "#1BDCA2", marginLeft: "10px", fontSize: 13, marginTop: 20, textAlign: 'start' }}>$837K</p>
                                                                                <p style={{ color: "#A5A1A1", marginLeft: "10px", marginTop: "-14px" }}>Revenue</p>
                                                                      </div>
                                                            </div>
                                                  </div>
                                        </div>

                              </div>
                              
</div>
          )}
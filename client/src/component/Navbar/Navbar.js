import React from 'react'
import "./Navbar.css"

import SearchIcon from "../../Icons/search.png";
import CartIcon from "../../Icons/shopping-cart.png";
import Notification from "../../Icons/icons8-notification-64.png";
import ProfileImage from "../../Icons/Profile (2).jpg";
import Page from '../Page/Page';

function Navbar() {
  return (
    <div className='navbar'>
    <div className='subnavbar'>
    <div>
      <p style={{color:"white"   , marginLeft:"14px" , marginTop:12}}>Admin</p>
    </div>
    <div style={{display:"flex" , alignItems:"center"}}>
      <img src={`${SearchIcon}`} className="navbarIcons" alt="" />
      <img src={`${CartIcon}`} className="navbarIcons" alt="" />
      <img src={`${Notification}`} className="navbarIcons" alt="" />
      <div style={{display:"flex" , alignItems:'center' , marginLeft:20}}>
        <img style={{width:30 , height:30 , marginRight:4 , borderRadius:"50%"}} src={`${ProfileImage}`} alt="" />
        <p style={{color:"#1ED97B" , marginRight:20}}>bhartendu</p>
      </div>
    </div>
    </div>
    
    <Page/>
  </div>
  
  )
}

export default Navbar


import React from 'react'
import Navbar from '../Navbar/Navbar'
import Sidebar from '../Sidebar/Sidebar'
import "./Home.css"

function Home() {
  return (
    <div className='maincontainer'>
        <Sidebar/>
        <Navbar/>
    </div>
  )
}

export default Home